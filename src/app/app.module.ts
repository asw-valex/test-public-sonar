import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HelloComponent } from './hello/hello.component';
import { TimeComponent } from './time/time.component';
import { SongComponent } from './song/song.component';

@NgModule({
  declarations: [AppComponent, HelloComponent, TimeComponent, SongComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
