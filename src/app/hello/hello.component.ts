import { Component } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css'],
})
export class HelloComponent {
  public counter: number = 0;

  increaseCounter() {
    this.counter++;
  }

  isEven() {
    return this.counter % 2 === 0;
  }
}
