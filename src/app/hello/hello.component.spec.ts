import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HelloComponent } from './hello.component';

describe('HelloComponent', () => {
  let component: HelloComponent;
  let fixture: ComponentFixture<HelloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HelloComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HelloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('increaseCounter', () => {
    it('should increase the counter variable', () => {
      expect(component.counter).toBe(0);
      component.increaseCounter();
      expect(component.counter).toBe(1);
    });
  });

  describe('isEven', () => {
    it('should return true when counter is even', () => {
      component.increaseCounter();
      component.increaseCounter();
      expect(component.counter).toBe(2);
      expect(component.isEven()).toBeTrue();
    });

    it('should return false when counter is odd', () => {
      component.increaseCounter();
      expect(component.counter).toBe(1);
      expect(component.isEven()).toBeFalse();
    });
  });
});
