import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SongComponent } from './song.component';

describe('SongComponent', () => {
  let component: SongComponent;
  let fixture: ComponentFixture<SongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SongComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getYear', () => {
    it('should return full year if it is greater than 1999', () => {
      component.song.year = 2000;
      expect(component.getYear()).toBe('2000');

      component.song.year = 2022;
      expect(component.getYear()).toBe('2022');
    });

    it('should return last to digits if is lesser than 2000', () => {
      component.song.year = 1999;
      expect(component.getYear()).toBe("'99");

      component.song.year = 1987;
      expect(component.getYear()).toBe("'87");
    });
  });
});
