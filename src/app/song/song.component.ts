import { Component } from '@angular/core';

@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.css'],
})
export class SongComponent {
  public song: { title: string; author: string; year: number };

  constructor() {
    this.song = {
      title: 'Someone Like You',
      author: 'Adele',
      year: 2011,
    };
  }

  getTitle(): string {
    return this.song.title.toUpperCase();
  }

  getAuthor(): string {
    return this.song.author.toLowerCase();
  }

  getYear(): string {
    return this.song.year < 2000 ? "'" + this.song.year.toString().substr(2) : this.song.year.toString();
  }
}
