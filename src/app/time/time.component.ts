import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.css'],
})
export class TimeComponent {
  // @ts-ignore
  @ViewChild('date') date: ElementRef<HTMLElement>;
  // @ts-ignore
  @ViewChild('time') time: ElementRef<HTMLElement>;

  today: number = Date.now();

  showDate() {
    this.date.nativeElement.classList.toggle('show');
  }

  showTime() {
    this.time.nativeElement.classList.toggle('show');
  }
}
