import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeComponent } from './time.component';

describe('TimeComponent', () => {
  let component: TimeComponent;
  let fixture: ComponentFixture<TimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TimeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('showDate', () => {
    it('should show/hide date span', () => {
      expect(component.date?.nativeElement.classList.contains('show')).toBeFalse();

      component.showDate();
      expect(component.date?.nativeElement.classList.contains('show')).toBeTrue();

      component.showDate();
      expect(component.date?.nativeElement.classList.contains('show')).toBeFalse();
    });
  });
});
