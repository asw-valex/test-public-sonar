import { Component } from '@angular/core';

@Component({
  selector: 'lib-foobar',
  template: `
    <p>
      foobar works!
      <button (click)="getFoo()">{{ lastDate | date }}</button>
    </p>
  `,
  styles: [],
})
export class FoobarComponent {
  public lastDate: Date = new Date();

  getFoo() {
    this.lastDate = new Date();
  }
}
