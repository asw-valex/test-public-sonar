import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FoobarService {
  getFoo(): string {
    return 'foo';
  }

  getBar(): string {
    return Date.now() % 2 === 0 ? 'foo' : 'bar';
  }
}
