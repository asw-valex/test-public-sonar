import { TestBed } from '@angular/core/testing';

import { FoobarService } from './foobar.service';

describe('FoobarService', () => {
  let service: FoobarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FoobarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
